import DelugeApi from 'deluge-api';
import prettysize from 'prettysize';

var sizeLimit = process.env.DELUGE_SIZE * 1024 * 1024 * 1024;

var HOSTNAME = process.env.DELUGE_HOST;
var PASSWORD = process.env.DELUGE_PASSWORD;

var deluge = new DelugeApi({
    host: HOSTNAME,
    https: true,
});



deluge.getSystemMethods()
    .then((system) => {
        return system.auth.check_session()
            .then((authed) => {
                if (!authed) {
                    return new Promise((resolve, reject) => {
                        system.auth.login([PASSWORD])
                            .then((authed) => {
                                if (authed) {
                                    resolve();
                                } else {
                                    reject('Invalid password');
                                }
                            }).catch(reject);
                    })
                    return Promise.resolve();
                }
            })
            .then(() => {
                return system.daemon.info();
            })
            .then((daemonVersion) => {
                console.log('daemonInfo: ' + daemonVersion);
                return checkSize(system);
            });
    }).catch((err) => {
        console.log(err);
        console.log('Errors: ' + JSON.stringify(err));
    });

function checkSize(system) {
    var params = [['name', 'time_added', 'total_size'], {}];
    return system.web.update_ui(params)
        .then((uiUpdate) => {
            console.log("UI Update");
            console.log("Free space: %s, need to free %s", prettysize(uiUpdate.stats.free_space), prettysize(sizeLimit - uiUpdate.stats.free_space));
            if (uiUpdate.stats.free_space < sizeLimit) {

                let spaceToFree = sizeLimit - uiUpdate.stats.free_space;
                console.log("Need to free " + prettysize(spaceToFree));

                let torrents = [];
                Object.keys(uiUpdate.torrents).forEach((id) => {
                    uiUpdate.torrents[id].id = id;
                    torrents.push(uiUpdate.torrents[id]);
                });
                torrents.sort((a, b) => {
                    return a.time_added - b.time_added;
                });
                let numberSize = 0;
                let deletionPromises = [];
                return torrents.reduce((last, torrent) => {
                    if (numberSize < spaceToFree) {
                        numberSize += torrent.total_size;
                        console.log("Freeing %s space by removing %s ", prettysize(torrent.total_size), torrent.name, torrent.id);
                        return last.then(() => system.core.remove_torrent([torrent.id, true]));
                    }
                    return last;
                }, Promise.resolve());
            }
        });
}
